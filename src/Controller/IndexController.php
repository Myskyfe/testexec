<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $users = $entityManager->getRepository(User::class)->findAll();
        $fetchedUsers = [];

        foreach($users as $key => $user){
            $fetchedUsers[$key] = [
                'id' => $user->getId(),
                'firstName' => $user->getFirstName(),
                'lastName' => $user->getLastName(),
                'age' => $user->getAge(),
                'products' => $user->getProducts()->getValues(),
            ];
        }

        return $this->render('index/index.html.twig', [
            'users' => $fetchedUsers,
            'controller_name' => 'IndexController',
        ]);
    }
}
