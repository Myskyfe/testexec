<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\AddProductType;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    #[Route('/product', name: 'app_product')]
    public function index(Request                $request,
                          EntityManagerInterface $entityManager): Response
    {
        $recievedUsers = $entityManager->getRepository(User::class)->findAll();
        $users = [];

        $users['-- Select user'] = -1;
        foreach ($recievedUsers as $user) {
            $users[$user->getFirstName() . " " . $user->getLastName()] = $user->getId();
        }

        $form = $this->createForm(AddProductType::class, $users, [
            'data_class' => null
        ]);

        $this->ajaxProduct($request, $entityManager, $form);

        return $this->render('product/index.html.twig', [
            'add_product' => $form->createView(),
            'controller_name' => 'ProductController',
        ]);
    }

    #[Route('/product/ajax')]
    public function ajaxProduct(Request                $request,
                                EntityManagerInterface $entityManager): Response
    {
        if ($request->isMethod('POST')) {
            $data = $request->request->all();

            $product = new Product();

            $product->setName($data['name'] != "" ? $data['name'] : null);
            $product->setDescription($data['description']);
            $product->setPrice(floatval($data['price']));

            $user = $data['user'] != "-1" ? $entityManager->getRepository(User::class)->findOneBy([
                'id' => $data['user']
            ]) : "";

            $product->setUser($user);

            $entityManager->persist($product);
            $entityManager->flush();

            return new Response('
                <span class="success">Product ' . $product->getName() . ' created successfully!</span>
            ');
        }

        //if form is not submitted by this route
        return new Response(
            $this->redirectToRoute('app_product')
        );
    }
}
