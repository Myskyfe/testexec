<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AddUserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/user', name: 'app_user')]
    public function index(Request $request,
                          EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AddUserType::class);

        return $this->render('user/index.html.twig', [
            'add_user_form' => $form->createView(),
            'controller_name' => 'UserController',
        ]);
    }

    #[Route('/user/ajax')]
    public function ajaxUser(Request                $request,
                                EntityManagerInterface $entityManager): Response
    {
        if ($request->isMethod('POST')) {
            $data = $request->request->all();

            $user = new User();

            $user->setFirstName($data['first_name']);
            $user->setLastName($data['last_name']);
            $user->setAge(intval($data['age']));

            $entityManager->persist($user);
            $entityManager->flush();

            return new Response('
                <span class="success">User ' . $user->getFirstName() . ' ' . $user->getLastName() . ' created successfully!</span>
            ');
        }

        //if form is not submitted by this route
        return new Response(
            $this->redirectToRoute('app_user')
        );
    }
}
