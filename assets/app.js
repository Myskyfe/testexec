import './styles/app.css';
import './bootstrap';

require('bootstrap');

const $ = require('jquery');

$(document).ready(function(){
    /* ADD USER AJAX EVENT */
    $("#add_user_Submit").on("click", function(event){
        event.preventDefault();

        let firstName = $('input[name="add_user[firstName]"]').val();
        let lastName = $('input[name="add_user[lastName]"]').val();
        let age = $('input[name="add_user[age]"]').val();

        $.ajax({
            url:        '/user/ajax',
            type:       'POST',
            async:      true,
            data: {
                first_name: firstName,
                last_name: lastName,
                age: age,
            },
            success: function(data) {
                $('.result').html(data);
            },
            error : function() {
                $('.result').html('<span class="error">Please fill in all fields correctly!</span>');
            }
        });
    });

    /* ADD PRODUCT AJAX EVENT */
    $("#add_product_Submit").on("click", function(event){
        event.preventDefault();

        let name = $('input[name="add_product[name]"]').val();
        let description = $('textarea[name="add_product[description]"]').val();
        let price = $('input[name="add_product[price]"]').val();
        let user = $('select[name="add_product[user]"]').val()

        $.ajax({
            url:        '/product/ajax',
            type:       'POST',
            async:      true,
            data: {
                name: name,
                description: description,
                price: price,
                user: user,
            },
            success: function(data) {
                $('.result').html(data);
            },
            error : function() {
                $('.result').html('<span class="error">Please fill in all fields correctly!</span>');
            }
        });
    });
});

